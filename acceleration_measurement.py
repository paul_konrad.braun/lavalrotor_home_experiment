import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_phone.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sp = measure_duration_in_s*sensor_settings_dict["frequency"] #sp=samplepoints
data = { sensor_settings_dict["ID"]:{ "acceleration_x": np.zeros(sp), "acceleration_y": np.zeros(sp), "acceleration_z": np.zeros(sp), "timestamp": np.zeros(sp)}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

start = time.time()
i = 0
while ((time.time()-start) < measure_duration_in_s) and i < sp:
   for i in range(sp):
       (a_x, a_y, a_z) = accelerometer.acceleration
       data[sensor_settings_dict["ID"]]["acceleration_x"][i] = a_x
       data[sensor_settings_dict["ID"]]["acceleration_y"][i] = a_y
       data[sensor_settings_dict["ID"]]["acceleration_z"][i] = a_z
       data[sensor_settings_dict["ID"]]["timestamp"][i] = time.time()
       i += 1

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
f = h5py.File(path_h5_file, "w")
rawdata = f.create_group("RawData")
g = rawdata.create_group(sensor_settings_dict["ID"])
for key in data[sensor_settings_dict["ID"]].keys():
    g.create_dataset( key, data=data[sensor_settings_dict["ID"]][key])
f.close()
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
